#ifndef ALGORITHMS_H_INCLUDED
#define ALGORITHMS_H_INCLUDED

namespace takg
{
    /*
        find searches for value in a container
        and returns the index or the container.end()
    */
    template <typename T, typename InputIterator>
    InputIterator find(const T& value, InputIterator begin, InputIterator end) throw()
    {
        while(begin != end)
        {
            if ( *begin == value)
            {
                break;
            }
            ++begin;
        }

        return begin;
    }

    /*
        exists searches for an item within the container
    */
    template <typename T, typename InputIterator>
    bool exists(const T& value, InputIterator begin, InputIterator end) throw()
    {
        return (takg::find(value, begin, end) != end);
    }

    /*
        transform - Input range to Output range
    */
    template<typename InputIterator,typename OutputIterator, typename UnaryOperation>
    OutputIterator transform(InputIterator start, InputIterator end, OutputIterator out, UnaryOperation uo)
    {
        while ( start != end )
        {
            *out = uo(*start);
            ++start;
            ++out;
        }
        return out;
    }

    /*
        transform - Input range1 op Input range 2 to Output range
    */
    template<typename InputIterator, typename OutputIterator, typename BinaryOperation>
    OutputIterator transform(InputIterator start1, InputIterator end1, InputIterator start2, OutputIterator out, BinaryOperation bo)
    {
        while ( start1 != end1 )
        {
            *out = bo(*start1,*start2);
            ++start1;
            ++start2;
            ++out;
        }
        return out;
    }

    /* START OF BINARY TREE FUNCTIONS */
    /*
        TODO: Move exceptions to more appropriate place
    */
    class BinaryTreeEmpty:public std::exception
    {
    public:
        BinaryTreeEmpty(){}
        ~BinaryTreeEmpty() throw() {}
        const char* what()
        {
            return "BinaryTreeEmpty: There is no root node.";
        }
    };

    /*
        Returns the number of nodes within Binary tree
    */
    template <typename const_iterator>
    int size(const_iterator it)
    {
        if (it.isNull())
        {
            return 0;
        }

        return (size(it.left())+size(it.right())+1);
    }

    /*
        Returns the minimum value within Binary tree
        TODO - Enhance this to use Binary Tree iterator only
    */
    template <typename T, typename const_iterator>
    const T& minValue(const_iterator it) throw(BinaryTreeEmpty)
    {
        if (it.isNull())
        {
            // throw exception
            throw BinaryTreeEmpty();
        }

        while (!it.left().isNull())
        {
            --it;
        }

        return *it;
    }

    /*
        Returns the max item in the binary tree
        TODO - enhance this to use Binary Tree iterator only
    */
    template <typename T, typename const_iterator>
    T& maxValue(const_iterator it) throw(BinaryTreeEmpty)
    {
        if (it.isNull())
        {
            // throw exception
            throw BinaryTreeEmpty();
        }

        while (!it.right().isNull())
        {
            ++it;
        }

        return *it;
    }

    /*
        Prints all the paths of the binary tree
        TODO - enhance this to use Binary Tree iterator only
    */
    template <typename T, typename const_iterator>
    void printPaths(const_iterator it, takg::Stack<T>& stackPath)
    {
        if (it.isNull())
        {
            return;
        }

        stackPath.push(*it);
        // print only if leaf node
        if (it.left().isNull() && it.right().isNull())
        {
            std::cout << "Path "<< stackPath << std::endl;
        }

        printPaths(it.left(), stackPath);
        printPaths(it.right(), stackPath);
        stackPath.pop();
        return;
    }

    /*
        Returns the depth of binary tree
        TODO - enhance this to use Binary Tree iterator only
    */
    template <typename const_iterator>
    int depth(const_iterator it, int depthLevel=0)
    {
        if (it.isNull())
        {
            // do not include the null node count
            return (depthLevel-1);
        }

        int depthLevelLeft = depth(it.left(), depthLevel+1);
        int depthLevelRight = depth(it.right(), depthLevel+1);

        if (depthLevel < depthLevelLeft)
        {
            depthLevel = depthLevelLeft;
        }
        if (depthLevel < depthLevelRight)
        {
            depthLevel = depthLevelRight;
        }

        return depthLevel;
    }

    /*
        Finds the item value in binary tree
        TODO - enhance this to use Binary Tree iterator only
    */
    template <typename T, typename const_iterator>
    bool find(const T& value, const_iterator it) throw()
    {
        if (it.isNull())
        {
            return false;
        }

        if (*it == value)
        {
            return true;
        }

        if (typename const_iterator::IteratorComparator()(value, *it))
        {
            return find(value, it.left());
        }
        else
        {
            return find(value, it.right());
        }

        return false;
    }

    /*
        Iterate in ascending order
        TODO - enhance this to use Binary Tree iterator only
    */
    template <typename Functor, typename const_iterator>
    void ascending(Functor func, const_iterator it) throw()
    {
        inOrder(func, it);
    }

    /*
        Iterate binary tree using inorder traversal
        TODO - enhance this to use Binary Tree iterator only
    */
    template <typename Functor, typename const_iterator>
    void inOrder(Functor func, const_iterator it) throw()
    {
        if (it.isNull())
        {
            return;
        }

        inOrder(func, it.left());
        func(*it);
        inOrder(func, it.right());
        return;
    }

    /*
        Iterate binary tree in descending order
        TODO - enhance this to use Binary Tree iterator only
    */
    template <typename Functor, typename const_iterator>
    void descending(Functor func, const_iterator it) throw()
    {
        if (it.isNull())
        {
            return;
        }

        descending(func, it.right());
        func(*it);
        descending(func, it.left());
        return;
    }

    /*
        Iterate binary tree in pre order
        TODO - enhance this to use Binary Tree iterator only
    */
    template <typename Functor, typename const_iterator>
    void preOrder(Functor& func, const_iterator it) throw()
    {
        if (it.isNull())
        {
            return;
        }

        func(*it);
        preOrder(func, it.left());
        preOrder(func, it.right());

        return;
    }

    /*
        Iterate binary tree in post order
        TODO - enhance this to use Binary Tree iterator only
    */
    template <typename Functor, typename const_iterator>
    void postOrder(Functor& func, const_iterator it) throw()
    {
        if (it.isNull())
        {
            return;
        }

        postOrder(func, it.right());
        postOrder(func, it.left());
        func(*it);

        return;
    }

    /*
        Returns true if path sum exists in binary tree
        TODO - enhance this to use Binary Tree iterator only
    */
    template <typename const_iterator>
    bool hasPathSum(const_iterator it, int sum)
    {
        if (it.isNull())
        {
            return (sum == 0);
        }

        if (hasPathSum(it.left(), sum - *it))
        {
            return true;
        }

        if (hasPathSum(it.right(), sum - *it))
        {
            return true;
        }

        return false;
    }/* END OF BINARY TREE FUNCTIONS */
}


#endif // ALGORITHMS_H_INCLUDED
