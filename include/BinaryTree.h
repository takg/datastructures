#ifndef BINARYTREE_H_INCLUDED
#define BINARYTREE_H_INCLUDED

#include <iostream>

namespace takg
{
    template <typename T, typename Comparator=std::less<T> >
    class BinaryTree
    {
        struct Node
        {
            T value;
            Node* left;
            Node* right;

            // copy right node
            Node(const T& right):
                value(right),
                left(NULL),
                right(NULL)
            {
            }

            // destructor
            ~Node()
            {
            }

            // assignment of right node
            Node& operator=(const T& right)
            {
                value = right.value;
                left = right = NULL;
            }
        };

        typedef Node* NodePtr;

    public:

        class iterator
        {
        public:
            typedef Comparator IteratorComparator;
            iterator(NodePtr pointer) :ptr(pointer)
            {
            }

            bool isNull() const
            {
                return (ptr == NULL);
            }

            bool isLeftNull() const
            {
                return (ptr->left == NULL);
            }

            bool isRightNull() const
            {
                return (ptr->right == NULL);
            }

            iterator left()
            {
                return iterator(ptr->left);
            }

            iterator right()
            {
                return iterator(ptr->right);
            }

            iterator& operator++()
            {
                ptr = ptr->right;
                return *this;
            }

            iterator& operator--()
            {
                ptr = ptr->left;
                return *this;
            }

            T& operator*()
            {
                return ptr->value;
            }

        private:
            NodePtr ptr;
        };

        class const_iterator
        {
        public:
            typedef Comparator IteratorComparator;
            typedef T Value;

            const_iterator(NodePtr pointer) :ptr(pointer)
            {
            }

            bool isNull() const
            {
                return (ptr == NULL);
            }

            bool isLeftNull() const
            {
                return (ptr->left == NULL);
            }

            bool isRightNull() const
            {
                return (ptr->right == NULL);
            }

            const_iterator left() const
            {
                return const_iterator(ptr->left);
            }

            const_iterator right() const
            {
                return const_iterator(ptr->right);
            }

            const_iterator& operator++()
            {
                ptr = ptr->right;
                return *this;
            }

            const_iterator& operator--()
            {
                ptr = ptr->left;
                return *this;
            }

            const T& operator*() const
            {
                return ptr->value;
            }

        private:
            NodePtr ptr;
        };

        // constructor
        BinaryTree() throw():head(NULL)
        {
        }

        // destructor
        ~BinaryTree() throw()
        {
            clear();
        }

        // delete all nodes under binary tree
        void clear()
        {
            deleteNode(head);
        }

        NodePtr allocateNode(const T& value)
        {
            return (new Node(value));
        }

        // operator < should be overriden for T
        // insert a value into binary tree
        T& insert(const T& value)
        {
            NodePtr node;
            if (head == NULL)
            {
                head = node = allocateNode(value);
            }
            else
            {
                NodePtr ptr = head;
                while (ptr != NULL)
                {
                    if (ptr->value == value)
                    {
                        node = ptr;
                        break; // the value is already set
                    }
                    else if (Comparator()(value, ptr->value))
                    {
                        if (ptr->left == NULL)
                        {
                            ptr->left = node = allocateNode(value);
                            break;
                        }
                        ptr = ptr->left;
                    }
                    else
                    {
                        if (ptr->right == NULL)
                        {
                            ptr->right = node = allocateNode(value);
                            break;
                        }
                        ptr = ptr->right;
                    }
                }
            }

            return node->value;
        }

        // copy binary tree
        BinaryTree& operator=(const BinaryTree& right)
        {
            this->~BinaryTree();
            clone(right.begin());
            return *this;
        }

        // clone the binary tree
        void clone(const_iterator it)
        {
            if (it.isNull())
            {
                return;
            }
            insert(*it);
            clone(it.left());
            clone(it.right());
            return;
        }

        // returns if the binary tree passed is equal to this
        bool operator==(BinaryTree& right)
        {
            return equals(this->head, right.head);
        }

        // return the root of binary tree
        iterator begin()
        {
            return iterator(head);
        }

        // return the root of binary tree
        const_iterator begin() const
        {
            return const_iterator(head);
        }

    private:
        /* NOTE: Private implementation is done using NodePtr rather than iterators */
        // returns true when both node structure and data resemble each other
        bool equals(NodePtr leftNode, NodePtr rightNode)
        {
            if (leftNode == NULL)
            {
                return (rightNode == NULL);
            }

            // if rightNode is null, left has data so not equals
            if (rightNode == NULL)
            {
                return false;
            }

            if (leftNode->value != rightNode->value)
            {
                return false;
            }

            if (!equals(leftNode->left, rightNode->left))
            {
                return false;
            }

            if (!equals(leftNode->right, rightNode->right))
            {
                return false;
            }

            return true;
        }

        // delete ALL nodes under a node pointer
        void deleteNode(NodePtr ptr)
        {
            if(ptr == NULL)
            {
                return;
            }

            deleteNode(ptr->left);
            deleteNode(ptr->right);

            delete ptr;
            head = NULL;

            return;
        }

        // no copy constructor
        BinaryTree(const BinaryTree& right);
        // head of the nodeptr is saved here
        NodePtr head;
    };
}

#endif // BINARYTREE_H_INCLUDED
