#ifndef DLIST_H_INCLUDED
#define DLIST_H_INCLUDED

#include <iostream>

namespace takg
{

    template<typename T>
    class DoubleList
    {
        /* private declaration of DoubleNode */
        struct DoubleNode
        {
            typedef DoubleNode& DNodeRef;
            typedef DoubleNode* DNodePtr;

            // constructor
            DoubleNode():
                next(NULL),
                prev(NULL)
            {
            }
            // copy constructor
            DoubleNode(const T& value):
                next(NULL),
                prev(NULL),
                data(value)
            {
            }
            // destructor
            ~DoubleNode() throw();
            // next node ptr
            DNodePtr next;
            // prev node ptr
            DNodePtr prev;
            // data
            T data;
        };
    public:
        typedef typename DoubleNode::DNodePtr DNodePtr;
        typedef typename DoubleNode::DNodeRef DNodeRef;

        class iterator
        {
        public:
            iterator(DNodePtr pointer=NULL):ptr(pointer)
            {
            }
            ~iterator()
            {
                ptr = NULL;
            }
            iterator(const iterator& it):ptr(it.ptr)
            {
            }
            iterator& operator=(const iterator& it)
            {
                if (this != &it)
                {
                    this->~iterator();
                    ptr = it.ptr;
                }
            }
            T& operator*()
            {
                return ptr->data;
            }
            T* operator->()
            {
                return &(ptr->data);
            }
            bool operator!=(const iterator& right)
            {
                return !(*this == right);
            }
            bool operator==(const iterator& right)
            {
                return (ptr == right.ptr);
            }
            iterator& operator++()
            {
                ptr = ptr->next;
                return *this;
            }
            iterator operator++(int)
            {
                iterator it(this);
                ptr = ptr->next;
                return it;
            }
            iterator& operator--()
            {
                ptr = ptr->prev;
                return *this;
            }
            iterator operator--(int)
            {
                iterator it(this);
                ptr = ptr->prev;
                return it;
            }
        private:
            DNodePtr ptr;
        };

        class const_iterator
        {
        public:
            const_iterator(DNodePtr pointer=NULL):ptr(pointer)
            {
            }
            ~const_iterator()
            {
                ptr = NULL;
            }
            const_iterator(const iterator& it):ptr(it.ptr)
            {
            }
            const_iterator& operator=(const const_iterator& it)
            {
                if (this != &it)
                {
                    this->~iterator();
                    ptr = it.ptr;
                }
            }
            const T& operator*() const
            {
                return ptr->data;
            }
            bool operator!=(const const_iterator& right) const
            {
                return !(*this == right);
            }
            bool operator==(const const_iterator& right) const
            {
                return (ptr == right.ptr);
            }
            const_iterator& operator++()
            {
                ptr = ptr->next;
                return *this;
            }
            const_iterator& operator--()
            {
                ptr = ptr->prev;
                return *this;
            }
            /* No Post increment please */
            /*
            const_iterator operator++(int)
            {
                const_iterator it(this);
                ptr = ptr->next;
                return it;
            }
            const_iterator operator--(int)
            {
                const_iterator it(this);
                ptr = ptr->prev;
                return it;
            }
            */
        private:
            DNodePtr ptr;
        };

        // constructor
        DoubleList():
            head(NULL),
            tail(NULL)
        {
        }
        DoubleList(const DoubleList& right):
            head(NULL),
            tail(NULL)
        {
            for(const_iterator it = right.begin(); it != right.end() ; ++it)
            {
                this->insert(*it);
            }
        }
        ~DoubleList()
        {
            head = NULL;
        }
        iterator begin()
        {
            return iterator(head);
        }
        const_iterator begin() const
        {
            return const_iterator(head);
        }
        iterator end()
        {
            return iterator();
        }
        const_iterator end() const
        {
            return const_iterator();
        }

        /*
            To allow more expressive statements, like
            list.insert("hello");
            We cannot have pass by reference unfortunately for unnamed variable.
        */
        iterator insert(const T value)
        {
            DNodePtr node = new DoubleNode(value);
            if ( head == NULL)
            {
                head = tail = node;
            }
            else
            {
                tail->next = node;
                node->prev = tail;
                tail = node;
            }

            return iterator(node);
        }

    private:
        DNodePtr head;
        DNodePtr tail;
    };

    template<typename T>
    std::ostream& operator<<(std::ostream& out, DoubleList<T>& list)
    {
        for (typename DoubleList<T>::iterator it = list.begin(); it != list.end() ; ++it)
        {
            out << " " << *it;
        }
        out << std::endl;
        return out;
    }
}

#endif // DLIST_H_INCLUDED
