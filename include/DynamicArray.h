#ifndef DYNAMICARRAY_H_INCLUDED
#define DYNAMICARRAY_H_INCLUDED

#include <exception>
#include <sstream>

/*
TODO:
1. make begin() to end() work
*/

namespace takg
{
    class IndexOutOfRange: public std::exception
    {
    public:
        IndexOutOfRange(const int index):index_(index)
        {
        }
        virtual const char* what()
        {
            std::stringstream ss;
            ss << "IndexOutOfRange::[" << index_ << "]";
            return ss.str().c_str();
        }
    private:
        int index_;
    };

    template <typename T>
    class DynamicArray
    {
    public:
        // constructor
        DynamicArray():
            ptr(NULL)
        {
            maxItems = 8;
            ptr = new T[maxItems];
        }

        // copy constructor
        DynamicArray(const DynamicArray& right)
        {
            copy(right);
        }

        // destructor
        ~DynamicArray() throw()
        {
            delete ptr;
            ptr = 0;
            maxItems = 0;
        }

        // assignment operator
        DynamicArray<T>& operator=(const DynamicArray& right)
        {
            if (this != &right)
            {
                copy(right);
            }
            return *this;
        }
        // size of dynamic array
        size_t size() const throw()
        {
            return maxItems;
        }
        // max items that can be saved in the exisitng data structure
        size_t reserve() const throw();

        // retrieve element at index
        T& operator[](const int index) throw(IndexOutOfRange)
        {
            return const_cast<T&>((static_cast<const DynamicArray<T>&>(*this))[index]);
        }

        // retrieve element at index
        const T& operator[](const unsigned int index) const throw(IndexOutOfRange)
        {
            if(index >= maxItems)
            {
                // resize the internal data structure to accomodate more memory request
                T* newPtr = new T[index*2];
                std::copy(ptr, ptr+maxItems , newPtr);
                // remove the previous memory and swap pointers
                delete ptr;
                ptr = newPtr;
                maxItems = index*2;
            }

            return ptr[index];
        }

    private:
        // copy from right
        void copy(const DynamicArray& right)
        {
            maxItems = right.maxItems;
            ptr = new T[maxItems];
            return;
        }
        // member datastructure storing the values
        mutable T* ptr;
        // max no of items that can be saved without
        // any change to internal datastructre length
        mutable size_t maxItems;
    };
}

#endif // DYNAMICARRAY_H_INCLUDED
