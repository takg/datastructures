#ifndef GRAPH_H_INCLUDED
#define GRAPH_H_INCLUDED

#include "DoubleList.h"

namespace takg
{
    template <typename T>
    class Graph
    {
    public:
        // forward declaring the iterator of the class;
        class iterator;
    private:
        // forward declaring Vertex for Edge struct
        struct Vertex;

        // private definition for an Edge is INTENTIONAL
        struct Edge
        {
        public:
            // source of the edge
            const T& source;
            // destination of the edge
            const T& destination;
            // is the edge visited
            bool visited;
            // weight given for the edge
            double weight;

            Edge(const T& sourceVal, const T& destinationVal):
                source(sourceVal),
                destination(destinationVal),
                visited(false),
                weight(0.0L)
            {
            }
            Edge(const Edge& right):
                source(right.source),
                destination(right.destination ),
                visited(right.visited),
                weight(right.weight)
            {
            }
            Edge& operator=(const Edge& right)
            {
                this->source = right.source;
                this->destination = right.destination;
                this->visited = right.visited;
                this->weight = right.weight;
                return *this;
            }
        };

        // private definition for Vertex is INTENTIONAL
        struct Vertex
        {
            T value;
            bool visited;

            Vertex(const T& val):
                value(val),
                visited(false)
            {
            }

            Vertex& operator=(const Vertex& right)
            {
                this->value = right.value;
                this->visited = right.visited;
            }
        };
    public:
        // building vocabulary
        typedef typename takg::DoubleList<Vertex>::iterator VertexIterator;
        typedef typename takg::DoubleList<Edge>::iterator EdgeIterator;
        class iterator
        {
        public:

            iterator(VertexIterator iter):it(iter)
            {
            }
            iterator(const iterator& right):it(right.it)
            {
            }
            ~iterator()
            {
            }
            iterator& operator=(const iterator& right)
            {
                this->it = right.it;
                return *this;
            }
            /*
                No post-increment operator provided
                Intention is cruel, but outcome is efficient code
            */
            iterator& operator++() throw()
            {
                ++it;
                return *this;
            }
            iterator& operator--() throw()
            {
                --it;
                return *this;
            }
            T& operator*() throw()
            {
                return (*it).value;
            }
            Vertex* operator->() throw()
            {
                return &(*it);
            }
            bool operator==(const iterator& right) throw()
            {
                return this->it == right.it;
            }
            bool operator!=(const iterator& right) throw()
            {
                return this->it != right.it;
            }
        private:
            VertexIterator it;
        };

        // returns the first item
        iterator begin()
        {
            return iterator(vertices.begin());
        }

        // returns the last item
        iterator end()
        {
            return iterator(vertices.end());
        }

        // default constuctor
        Graph()
        {
        }

        // copy from right
        Graph(const Graph& right)
        {
        }

        // assignment
        Graph& operator=(const Graph& right);

        iterator vertex(const T& value)
        {
            // add an edge only if it doesn't exist
            iterator it(takg::find(value, begin(), end()));
            if (it == vertices.end())
            {
                it = vertices.insert(Vertex(value));
            }

            return it;
        }

        // add a directed vertex
        double& edge(const T& source, const T& destination)
        {
            iterator itSource = vertex(source);
            iterator itDestination = vertex(destination);
            // add edge to next as this, only if not already added
            typename takg::DoubleList<Edge>::iterator it = edges.begin();

            while (it != edges.end())
            {
                if (it->destination == destination &&
                    it->source == source)
                {
                    break;
                }
            }
            // add an edge only if it doesn't exist
            if (it == edges.end())
            {
                it = edges.insert(Edge(source, destination));
            }

            return it->weight;
        }

        // depth first search algorithm
        void DFS(const T& startValue, const T& endValue)
        {
            iterator start(vertex(startValue));
            iterator end(vertex(endValue));
            return ;
        }

        // breadth first search algorithm
        void BFS(const T& startValue, const T& endValue)
        {
            return ;
        }

        // this function will show the isolated vertices
        void isolatedVertices()
        {
            for (iterator it = vertices.begin(); it != vertices.end() ; ++it)
            {
                if (it->edges.begin() == it->edges.end())
                {
                    std::cout << "Isolated vertex " << *it << std::endl;
                }
            }

            return;
        }

        template <typename FuncVertex, typename FuncEdge>
        void visitor(FuncVertex funcVertex, FuncEdge funcEdge)
        {
            for (typename takg::DoubleList<Vertex>::iterator itVertex = vertices.begin() ; itVertex != vertices.end() ; ++itVertex)
            {
                funcVertex(itVertex);
            }
            for (typename takg::DoubleList<Edge>::iterator itEdge = edges.begin() ; itEdge != edges.end() ; ++itEdge)
            {
                funcEdge(itEdge);
            }
            return;
        }

    private:

        // internal datastructure holding all the vertices
        takg::DoubleList<Vertex> vertices;
        // internal datastructure for edges
        takg::DoubleList<Edge> edges;
    };

    template <typename T, typename itVertex=typename Graph<T>::iterator>
    struct IteratorVertexPrint
    {
        void operator()(itVertex it)
        {
            std::cout << "Vertex " << *it << std::endl;
        }
    };

    template <typename T, typename itEdge=typename Graph<T>::iterator>
    struct IteratorEdgePrint
    {
        void operator()(itEdge it)
        {
            ;//std::cout << "Vertex " << *it << std::endl;
        }
    };
}


#endif // GRAPH_H_INCLUDED
