#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED

#include <utility>
#include "BinaryTree.h"

/*
TODO:
1. Add iterator support
2. Add size()
*/
namespace takg
{
    template <typename Key, typename Value>
    struct Pair
    {
        Pair(const Key& key, const Value& value):
            first(key),
            second(value)
        {
        }

        Pair(const Pair& right):
            first(right.first),
            second(right.second)
        {
        }

        // compare the key field only
        bool operator< (const Pair& right) const throw()
        {
            return (this->first < right.first);
        }

        // compare the key field only
        bool operator== (const Pair& right) const throw()
        {
            return (this->first == right.first);
        }

        // compare the key field only
        bool operator!= (const Pair& right) const throw()
        {
            return (this->first != right.first);
        }

        // compare the key field only
        Pair& operator= (const Pair& right) throw()
        {
            this->first = right.second;
            this->second = right.second;
            return *this;
        }

        Key first;
        Value second;
    };

    template <typename Key, typename Value, typename Container=BinaryTree<takg::Pair<Key, Value> > >
    class Map
    {
    public:

        // constructor
        Map()
        {
        }

        // destructr
        ~Map()
        {
            container.clear();
        }

        // copy map from right
        Map(const Map& right)
        {
            copy(right);
        }

        // assign map from right
        Map operator=(const Map& right)
        {
            if (this != &right)
            {
                ~Map();
                copy(right);
            }
            return *this;
        }

        // return the map's value
        const Value& operator[](const Key& key) const
        {
            return container.insert(Pair<Key, Value> (key, Value())).second;
        }

        // return the maps' value
        Value& operator[](const Key& key)
        {
            return container.insert(Pair<Key, Value> (key, Value())).second;
        }

    private:
        // the container
        Container container;
    };

    /* print to ostream  Map*/
    template <typename Key, typename Value>
    std::ostream& operator<< (std::ostream& out, const Map<Key, Value>& map)
    {
        //out << map;
        return out;
    }
}

#endif // MAP_H_INCLUDED
