#ifndef QUEUE_H
#define QUEUE_H

#include <deque>
#include <cstddef>

namespace takg
{
    // QueueOverflow is thrown when stack is out of memory
    class QueueOverflow: public std::exception
    {
        int _index;
    public:
        QueueOverflow(int index):_index(index){}
        virtual const char* what() const throw()
        {
            return "takg::QueueOverflow";
        }
    };

    // QueueUnderflow is thrown when stack is out of memory
    class QueueUnderflow: public std::exception
    {
    public:
        QueueUnderflow(){}
        virtual const char* what() const throw()
        {
            return "takg::QueueUnderflow";
        }
    };

    template <typename T, typename Container=std::deque<T> >
    /* Queue is not meant to be derived. Please note the non-virtual destructor */
    class Queue
    {
        public:
            Queue()
            {
            }

            // copy queue
            Queue(const Queue<T,Container>& right)
            {
                copy(right);
            }

            // note - destructor is non-virutal
            ~Queue() throw()
            {
            }

            // add element to queue
            void push_back(const T& item) throw(QueueOverflow)
            {
                _container.push_back(item);
                return;
            }

            // returns the first item out
            T pop_front() throw(QueueUnderflow)
            {
                if (this->empty())
                {
                    throw QueueUnderflow();
                }
                // .front() followed by pop_front would remove the element
                // as a result, item is copy by value i.e. a new copy
                T item = _container.front();
                _container.pop_front();
                return item; // again note that item is a local object so copy by value
            }

            // returns true if queue is empty
            bool empty() const throw()
            {
                return _container.empty();
            }

            // return the size of the queue
            size_t size() const throw()
            {
                return _container.size();
            }

            // assignment of queue
            Queue& operator=(const Queue& right)
            {
                this->_container.clear();
                copy(right);
                return *this;
            }

            class iterator
            {
            public:
                iterator(typename Container::iterator iter):it(iter)
                {
                }

                iterator& operator++()
                {
                    ++it;
                }

                T& operator*()
                {
                    return *it;
                }

                bool operator==(const iterator& right) const throw()
                {
                    return (this->it == right.it);
                }

                bool operator!=(const iterator& right) const throw()
                {
                    return !(this->it == right.it);
                }
            private:
                typename Container::iterator it;
            };

            class const_iterator
            {
            public:
                const_iterator(typename Container::const_iterator iter):it(iter)
                {
                }

                const_iterator& operator++()
                {
                    ++it;
                    return *this;
                }

                const T& operator*()
                {
                    return *it;
                }

                bool operator==(const const_iterator& right) const throw()
                {
                    return (this->it == right.it);
                }

                bool operator!=(const const_iterator& right) const throw()
                {
                    return !(this->it == right.it);
                }
            private:
                typename Container::const_iterator it;
            };

            const_iterator begin() const
            {
                return const_iterator(_container.begin());
            }

            iterator begin()
            {
                return iterator(_container.begin());
            }

            const_iterator end() const
            {
                return const_iterator(_container.end());
            }

            iterator end()
            {
                return iterator(_container.end());
            }
        private:
            // function used by copy and assignment to copy values from right
            void copy(const Queue& right)
            {
                typename Container::const_iterator it = right._container.begin();

                while ( it != right._container.end())
                {
                    this->push_back(*it);
                    ++it;
                }

                return;
            }

            // using the datastructure Container internally
            Container _container;
    };

    // debug
    template <typename T>
    std::ostream& operator<<(std::ostream& out, const takg::Queue<T>& queue) throw()
    {
        typename takg::Queue<T>::const_iterator it = queue.begin();

        while ( it != queue.end())
        {
            std::cout << *it << " ";
            ++it;
        }

        return out;
    }

}

#endif // QUEUE_H
