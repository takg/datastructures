#ifndef SLIST_H_INCLUDED
#define SLIST_H_INCLUDED

#include <iostream>

namespace takg
{
    template<typename T>
    class SingleList
    {
        // SingleNode has single pointer for moving forward
        struct SingleNode
        {
        public:
            typedef SingleNode* SNodePtr;
            typedef SingleNode& SNodeRef;
            // constructor with input data
            SingleNode(const T& inputData):
                data(inputData),
                next(NULL)
            {
            }
            // destructor
            ~SingleNode() throw()
            {
            }
            // clone prototype
            SingleNode* clone()
            {
                return new SingleNode(this->_data);
            }
            // data for the node
            T data;
            // pointer to the next node
            SingleNode* next;
        private:
            // Not providing copy constructor
            SingleNode(const SNodeRef );
            // Not providing assignment
            SNodeRef operator=(const SNodeRef);
        };
    public:
        typedef typename SingleNode::SNodePtr SNodePtr;
        typedef typename SingleNode::SNodeRef SNodeRef;

        class iterator
        {
            SNodePtr ptr;
        public:
            iterator(SNodePtr pointer=NULL):ptr(pointer)
            {
            }
            ~iterator()
            {
                ptr = NULL;
            }
            iterator(const iterator& it):ptr(it.ptr)
            {
            }
            iterator& operator=(const iterator& it)
            {
                if (this != &it)
                {
                    this->~iterator();
                    ptr = it.ptr;
                }
            }
            T& operator*()
            {
                return ptr->data;
            }
            bool operator!=(const iterator& right)
            {
                return !(*this == right);
            }
            bool operator==(const iterator& right)
            {
                return (ptr == right.ptr);
            }
            iterator& operator++()
            {
                ptr = ptr->next;
                return *this;
            }
            /* No post increment please */
            /*
            iterator operator++(int)
            {
                iterator it(this);
                ptr = ptr->next;
                return it;
            }
            */
        };

        class const_iterator
        {
            SNodePtr ptr;
        public:
            const_iterator(SNodePtr pointer=NULL):ptr(pointer)
            {
            }
            ~const_iterator()
            {
                ptr = NULL;
            }
            const_iterator(const const_iterator& it):ptr(it.ptr)
            {
            }
            const_iterator& operator=(const const_iterator& it)
            {
                if (this != &it)
                {
                    this->~const_iterator();
                    ptr = it.ptr;
                }
            }
            const T& operator*() const
            {
                return ptr->data;
            }
            bool operator!=(const const_iterator& right) const
            {
                return !(*this == right);
            }
            bool operator==(const const_iterator& right) const
            {
                return (ptr == right.ptr);
            }
            const_iterator& operator++()
            {
                ptr = ptr->next;
                return *this;
            }
            /* No Post increment please */
            /*
            const_iterator operator++(int)
            {
                const_iterator it(this);
                ptr = ptr->next;
                return it;
            }
            */
        };

        // constructor
        SingleList():head(NULL)
        {
        }
        // copy constructor
        SingleList(SingleList& right):head(NULL)
        {
            copy(right);
        }
        // assignment
        SingleList& operator=(SingleList& right)
        {
            if(this != right)
            {
                ~SingleList();
                copy(right);

            }
            return *this;
        }
        // destructor
        ~SingleList()
        {
            SNodePtr ptr=NULL;

            while(head != NULL)
            {
                ptr = head->next;
                delete head;
                head = ptr;
            }
        }
        // adding a node to the list
        void insert(const T& value)
        {
            SNodePtr node = new SingleNode(value);
            // iterate till the last node and then insert the node at the end
            if (head == NULL)
            {
                head = node;
            }
            else
            {
                SNodePtr ptr = head;
                while(ptr->next != NULL)
                {
                    ptr = ptr->next;
                }
                ptr->next = node;
            }

            return;
        }
        iterator begin()
        {
            return iterator(head);
        }
        iterator end()
        {
            return iterator();
        }
        const_iterator begin() const
        {
            return const_iterator(head);
        }
        const_iterator end() const
        {
            return const_iterator();
        }
    private:
        // copy
        void copy(SingleList& right)
        {
            for(iterator it = right.begin(); it != right.end() ; ++it)
            {
                this->insert(*it);
            }

            return;
        }
        // head of the single linked list
        SNodePtr head;
    };

    // printing all the node's data
    template<typename T>
    std::ostream& operator<<(std::ostream& out, const SingleList<T>& list) throw()
    {
        std::cout << "SingleList elements::" ;
        typename SingleList<T>::const_iterator it = list.begin();

        while(it != list.end())
        {
            std::cout << " " << *it;
            ++it;
        }

        return out;
    }
}

#endif // SLIST_H_INCLUDED
