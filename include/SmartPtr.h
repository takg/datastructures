#ifndef SMARTPTR_H_INCLUDED
#define SMARTPTR_H_INCLUDED

namespace takg
{
    template <typename T>
    class SmartPtr
    {
    public:
        // default constructor
        SmartPtr();
        // explicit constructor
        explicit SmartPtr(T* ptr);
        // copy constructor
        SmartPtr(SmartPtr<T>& right);
        // destructor
        ~SmartPtr();
        // assignment operator
        SmartPtr& operator=(SmartPtr<T>& right);
        // get the handler
        T* get();
    private:
        // copy resource from right
        void copy(SmartPtr<T>& right);
        // the shared resource
        T* ptr_;
        // the shared count
        int* ptrCount_;
    };

    template <typename T>
    inline SmartPtr<T>::SmartPtr():
        ptr_(NULL),
        ptrCount_(NULL)
    {
    }

    template <typename T>
    inline SmartPtr<T>::SmartPtr(T* ptr):
        ptr_(ptr),
        ptrCount_(NULL)
    {
        ptrCount_ = new int();
        *ptrCount_ = 1;
    }

    template <typename T>
    inline SmartPtr<T>::SmartPtr(SmartPtr<T>& right)
    {
        copy(right);
    }

    template <typename T>
    inline SmartPtr<T>::~SmartPtr()
    {
        if (ptrCount_)
        {
            --(*ptrCount_);
            if ( (*ptrCount_) == 0)
            {
                delete ptr_;
                ptr_ = 0;
                delete ptrCount_;
                ptrCount_ = 0;
            }
        }
    }

    template <typename T>
    inline SmartPtr<T>& SmartPtr<T>::operator=(SmartPtr<T>& right)
    {
        copy(right);
        return *this;
    }

    template <typename T>
    inline T* SmartPtr<T>::get()
    {
        return ptr_;
    }

    template <typename T>
    inline void SmartPtr<T>::copy(SmartPtr<T>& right)
    {
        ++(*right.ptrCount_);
        ptrCount_ = right.ptrCount_;
        ptr_ = right.ptr_;
    }
}

#endif // SMARTPTR_H_INCLUDED
