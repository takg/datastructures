#ifndef STACK_H
#define STACK_H

#include <vector>
#include <cstddef>
#include <exception>

namespace takg
{
    // StackOverflow is thrown when stack is out of memory
    class StackOverflow: public std::exception
    {
        int _index;
    public:
        StackOverflow(int index):_index(index){}
        virtual const char* what() const throw()
        {
            return "takg::StackOverflow";
        }
    };

    // StackUnderflow is thrown when there is no item to display for pop / top
    class StackUnderflow: public std::exception
    {
    public:
        StackUnderflow(){}
        virtual const char* what() const throw()
        {
            return "takg::StackUnderflow";
        }
    };

    /* start of Stack class */
    template <typename T, typename Container=std::vector<T> >
    class Stack
    {
        public:
            // constructor
            Stack()
            {
            }

            // copy of stack
            Stack(const Stack<T,Container>& right)
            {
                copy(right);
            }

            // destructor
            ~Stack() throw()
            {
            }

            // add an item to the stack
            void push(const T& item) throw(StackOverflow)
            {
                try
                {
                    _container.push_back(item);
                }
                catch (std::exception& e)
                {
                    std::cerr << e.what() << std::endl;
                }
            }

            //remove an item from the stack
            const T pop() throw (StackUnderflow)
            {
                if (_container.size() == 0)
                {
                    throw StackUnderflow();
                }

                T& item = _container.back();
                _container.pop_back();

                return item;
            }

            // return the top of the stack
            const T& top() const throw(StackUnderflow)
            {
                if (this->empty())
                {
                    throw StackUnderflow();
                }
                return _container.back();
            }

            // return the top of the stack
            T& top() throw(StackUnderflow)
            {
                if (this->empty())
                {
                    throw StackUnderflow();
                }
                return _container.back();
            }

            // returns try if stack is empty
            bool empty() const throw()
            {
                return _container.empty();
            }

            // returns the number of items
            std::size_t size() const throw()
            {
                return _container.size();
            }

            // assignment operator
            Stack<T,Container>& operator=(const Stack<T,Container>& right)
            {
                this->_container.clear();
                copy(right);
                return *this;
            }

            class iterator
            {
            public:
                iterator(typename Container::iterator iter):it(iter)
                {
                }

                iterator& operator++()
                {
                    ++it;
                }

                T& operator*()
                {
                    return *it;
                }

                bool operator==(const iterator& right) const throw()
                {
                    return (this->it == right.it);
                }

                bool operator!=(const iterator& right) const throw()
                {
                    return !(this->it == right.it);
                }
            private:
                typename Container::iterator it;
            };

            class const_iterator
            {
            public:
                const_iterator(typename Container::const_iterator iter):it(iter)
                {
                }

                const_iterator& operator++()
                {
                    ++it;
                    return *this;
                }

                const T& operator*()
                {
                    return *it;
                }

                bool operator==(const const_iterator& right) const throw()
                {
                    return (this->it == right.it);
                }

                bool operator!=(const const_iterator& right) const throw()
                {
                    return !(this->it == right.it);
                }
            private:
                typename Container::const_iterator it;
            };

            const_iterator begin() const
            {
                return const_iterator(_container.begin());
            }

            iterator begin()
            {
                return iterator(_container.begin());
            }

            const_iterator end() const
            {
                return const_iterator(_container.end());
            }

            iterator end()
            {
                return iterator(_container.end());
            }

        private:

            // copy function is used to copy values from right in copy constructor and assigment operator
            void copy(const Stack& right)
            {
                typename Container::const_iterator it = right._container.begin();

                while ( it != right._container.end())
                {
                    this->push(*it);
                    ++it;
                }
                return;
            }

            // datastructure Container stores the values internally
            Container _container;
    };


    // print
    template <typename T>
    std::ostream& operator<<(std::ostream& out, const takg::Stack<T>& stack) throw()
    {
        typename takg::Stack<T>::const_iterator it = stack.begin();

        while (it != stack.end())
        {
            out << " " << *it;
            ++it;
        }

        return out;
    }

    /* end of Stack class */
}

#endif // STACK_H
