#include <iostream>
#include "unittests/QueueTest.h"
#include "unittests/StackTest.h"
#include "unittests/SingleListTest.h"
#include "unittests/DynamicArrayTest.h"
#include "unittests/DoubleListTest.h"
#include "unittests/SmartPtrTest.h"
#include "unittests/BinaryTreeTest.h"
#include "unittests/MapTest.h"
#include "unittests/GraphTest.h"

int main()
{
    std::cout << "Hello! Welcome to the world of DataStructures... " << std::endl;
    std::cout << "************************" << std::endl;
    StackTest();
    std::cout << "************************" << std::endl;
    QueueTest();
    std::cout << "************************" << std::endl;
    SingleListTest();
    std::cout << "************************" << std::endl;
    DynamicArrayTest();
    std::cout << "************************" << std::endl;
    DListTest();
    std::cout << "************************" << std::endl;
    SmartPtrTest();
    std::cout << "************************" << std::endl;
    BinaryTreeTest();
    std::cout << "************************" << std::endl;
    MapTest();
    std::cout << "************************" << std::endl;
    GraphTest();
    std::cout << "************************" << std::endl;
    std::cout << "Thank you " << std::endl;
    return 0;
}
