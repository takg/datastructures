#ifndef BINARYTREETEST_H_INCLUDED
#define BINARYTREETEST_H_INCLUDED

#include "../include/BinaryTree.h"
#include "../include/Algorithms.h"
#include <algorithm>

template <typename T>
struct printToConsole
{
    void operator()(const T& value)
    {
        std::cout << " " << value;
        return;
    }
};

void BinaryTreeTest()
{
    std::cout << "Binary Tree Operation" << std::endl;
    takg::BinaryTree<int> tree;
    takg::BinaryTree<int> copy;
    for (int nLoop = 0 ; nLoop < 10 ; ++nLoop )
    {
        const int value = std::rand()%99;
        tree.insert(value);
        copy.insert(value);
    }

    printToConsole<int> printInt;
    printToConsole<std::string> printString;

    takg::Stack<int> stackPath;
    takg::printPaths(tree.begin(), stackPath);
    std::cout << "Binary tree<int> nodes in-order: ";
    inOrder(printInt, tree.begin());
    std::cout << std::endl;
    std::cout << "Binary tree<int> nodes pre-order: ";
    preOrder(printInt, tree.begin());
    std::cout << std::endl;
    std::cout << "Binary tree<int> nodes post-order: ";
    takg::postOrder(printInt, tree.begin());
    std::cout << std::endl;
    std::cout << "Binary tree<int> nodes ascending: ";
    takg::ascending(printInt, tree.begin());
    std::cout << std::endl;
    std::cout << "Binary tree<int> nodes descending: ";
    takg::descending(printInt, tree.begin());
    std::cout << std::endl;
    std::cout << "Binary tree<int> find 41:"<<takg::find(41, tree.begin()) << std::endl;
    std::cout << "Binary tree<int> find 11:"<<takg::find(11, tree.begin()) << std::endl;
    std::cout << "Binary tree<int> find 13:"<<takg::find(13, tree.begin()) << std::endl;
    std::cout << "Binary tree<int> size:"<<takg::size(tree.begin()) << std::endl;
    std::cout << "Binary tree<int> min:"<<takg::minValue<int, takg::BinaryTree<int>::iterator >(tree.begin()) << std::endl;
    std::cout << "Binary tree<int> max:"<<takg::maxValue<int, takg::BinaryTree<int>::iterator >(tree.begin()) << std::endl;
    std::cout << "Binary tree<int> depth:"<<takg::depth(tree.begin()) << std::endl;
    std::cout << "Binary tree<int> compare: " << (tree == copy) << std::endl;
    copy.insert(1);
    std::cout << "Binary tree<int> compare: " << (tree == copy) << std::endl;

    takg::BinaryTree<std::string> stringTree;
    stringTree.insert("Beta");
    stringTree.insert("Alpha");
    stringTree.insert("Gamma");
    stringTree.insert("Delta");
    std::cout << "Binary tree<std::string> nodes ascending: ";
    takg::ascending(printString, stringTree.begin());
    std::cout << std::endl;
    std::cout << "Binary tree<std::string> nodes descending: ";
    takg::descending(printString, stringTree.begin());
    std::cout << std::endl;
    std::cout << "Binary tree<std::string> depth: " << takg::depth(stringTree.begin()) << std::endl;
    takg::BinaryTree<std::string> stringCopy;
    stringCopy = stringTree;
    takg::Stack<std::string> stack;
    printPaths(stringCopy.begin(), stack);

    return;
}

#endif // BINARYTREETEST_H_INCLUDED
