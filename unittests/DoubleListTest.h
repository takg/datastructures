#ifndef DLISTTEST_H_INCLUDED
#define DLISTTEST_H_INCLUDED

#include "../include/DoubleList.h"

void DListTest()
{
    std::cout << "Double List Operation" << std::endl;
    takg::DoubleList<int> list;
    list.insert(1);
    list.insert(2);
    list.insert(3);
    list.insert(4);
    list.insert(5);
    std::cout << "DoubleList<int> elements ";
    std::cout << list;
    return;
}

#endif // DLISTTEST_H_INCLUDED
