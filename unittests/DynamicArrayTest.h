#ifndef DYNAMICARRAYTEST_H_INCLUDED
#define DYNAMICARRAYTEST_H_INCLUDED

#include "../include/DynamicArray.h"

void DynamicArrayTest()
{
    std::cout << "DynamicArray Test" << std::endl;
    takg::DynamicArray<int> array;
    const int maxSize = 65;

    for (int nLoop = 0 ; nLoop < maxSize ; ++nLoop)
    {
        array[nLoop] = maxSize - nLoop;
    }

    std::cout << "Print Array details::";
    for (int nLoop = 0 ; nLoop < maxSize ; ++nLoop)
    {
        std::cout << " " << array[nLoop];
    }

    std::cout << std::endl;

    return;
}

#endif // DYNAMICARRAYTEST_H_INCLUDED
