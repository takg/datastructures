#ifndef GRAPHTEST_H_INCLUDED
#define GRAPHTEST_H_INCLUDED

#include "../include/Graph.h"

void GraphTest()
{
    std::cout << "Graph Operation" << std::endl;

    takg::Graph<std::string> placesGraph;
    const std::string HYDERABAD("Hyderabad");
    const std::string BANGALORE("Bangalore");
    const std::string CHENNAI("Chennai");
    const std::string NEW_DELHI("New Delhi");
    const std::string KOLKATTA("Kolkatta");
    const std::string MUMBAI("Mumbai");
    placesGraph.edge(HYDERABAD, BANGALORE) = 7;
    placesGraph.edge(BANGALORE, CHENNAI) = 5;
    placesGraph.edge(CHENNAI, NEW_DELHI) = 10;
    placesGraph.edge(NEW_DELHI, KOLKATTA) = 11;
    placesGraph.edge(KOLKATTA, MUMBAI) = 9;

    takg::IteratorVertexPrint<std::string> vertex;
    takg::IteratorEdgePrint<std::string> edge;

    //placesGraph.visitor(vertex,edge);

    return;
}

#endif // GRAPHTEST_H_INCLUDED
