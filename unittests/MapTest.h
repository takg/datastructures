#ifndef MAPTEST_H_INCLUDED
#define MAPTEST_H_INCLUDED

#include "../include/Map.h"

void MapTest()
{
    std::cout << "Map operation tests" << std::endl;
    takg::Map<int, double> mapDouble;
    mapDouble[1] = 2.0;
    mapDouble[2] = 3.5;
    std::cout << "MapDouble[1]: " << mapDouble[1] << std::endl;
    std::cout << "MapDouble[2]: " << mapDouble[2] << std::endl;

    takg::Map<int, std::string> mapString;
    mapString[1] = std::string("alpha");
    mapString[2] = std::string("beta");
    mapString[3] = std::string("gamma");
    mapString[4] = std::string("delta");
    for ( int nLoop = 1 ; nLoop < 5 ; ++nLoop)
    {
        std::cout << "mapString[" << nLoop << "]: " << mapString[nLoop] << std::endl;
    }


    return;
}

#endif // MAPTEST_H_INCLUDED
