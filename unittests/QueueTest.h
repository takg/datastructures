#ifndef QUEUETEST_H_INCLUDED
#define QUEUETEST_H_INCLUDED

#include "../include/Queue.h"

void QueueTest()
{
    std::cout << "Queue Operation" << std::endl;

    try
    {
        takg::Queue<int> queueInt;
        takg::Queue<std::string> queueString;
        queueInt.push_back(6);
        queueInt.push_back(7);
        queueInt.push_back(8);
        queueInt.push_back(9);
        queueInt.push_back(10);

        queueString.push_back("a");
        queueString.push_back("b");
        queueString.push_back("c");
        queueString.push_back("d");
        queueString.push_back("e");

        std::cout << "Queue Copy Operation" << std::endl;
        takg::Queue<std::string> copyQueue(queueString);
        std::cout << "Queue after copy " << copyQueue << std::endl;
        copyQueue.pop_front();

        std::cout << "Queue Assignment Operation" << std::endl;
        takg::Queue<std::string> assignmentQueue;
        assignmentQueue = copyQueue;
        std::cout << "Queue items after assignment " << assignmentQueue << std::endl;


        std::cout << "Queue int elements are ";
        std::cout << queueInt.pop_front() << " ";
        std::cout << queueInt.pop_front() << " ";
        std::cout << queueInt.pop_front() << " ";
        std::cout << queueInt.pop_front() << " ";
        std::cout << queueInt.pop_front() << " " << std::endl;
        //queue.pop_front();

        std::cout << "Queue int elements are ";
        std::cout << queueString.pop_front() << " ";
        std::cout << queueString.pop_front() << " ";
        std::cout << queueString.pop_front() << " ";
        std::cout << queueString.pop_front() << " ";
        std::cout << queueString.pop_front() << " " << std::endl;
        queueString.pop_front();
    }
    catch(const takg::QueueOverflow& e)
    {
        std::cout << "Exception QueueOverflow caught:: " << e.what() << std::endl;
    }
    catch(const takg::QueueUnderflow& e)
    {
        std::cout << "Exception QueueUnderflow caught:: " << e.what() << std::endl;
    }
    catch(const std::exception& e)
    {
        std::cout << "Exception exception caught:: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "Exception All caught:: " << std::endl;
    }
}


#endif // QUEUETEST_H_INCLUDED
