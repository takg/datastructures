#ifndef SLISTTEST_H_INCLUDED
#define SLISTTEST_H_INCLUDED

#include "../include/SingleList.h"
#include "../include/Algorithms.h"

void SingleListTest()
{
    std::cout << "Single List Operation" << std::endl;
    takg::SingleList<int> list;
    list.insert(11);
    list.insert(12);
    list.insert(13);
    list.insert(14);
    list.insert(15);
    list.insert(16);
    std::cout << "SingleList print int nodes" << list << std::endl;

    takg::SingleList<std::string> listString;
    listString.insert("This");
    listString.insert("is");
    listString.insert("a");
    listString.insert("list");
    listString.insert("of");
    listString.insert("string");
    std::cout << "SingleList print string nodes" << listString << std::endl;

    takg::SingleList<std::string> cloneList(listString);
    std::cout << "SingleList string clone:::" << cloneList << std::endl;
    std::cout << "'a' exists " << takg::exists("a", cloneList.begin(), cloneList.end()) << std::endl;
    std::cout << "'of' does not exist " << takg::exists("is", cloneList.begin(), cloneList.end()) << std::endl;
    std::cout << "'the' does not exist " << takg::exists("the", cloneList.begin(), cloneList.end()) << std::endl;

    return;
}

#endif // SLISTTEST_H_INCLUDED
