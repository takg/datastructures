#ifndef SMARTPTRTEST_H_INCLUDED
#define SMARTPTRTEST_H_INCLUDED

#include "../include/SmartPtr.h"

void SmartPtrTest()
{
    std::cout << "SmartPtr testing" << std::endl;
    takg::SmartPtr<int> ptr(new int(5));
    std::cout << "Pointer Handler:" << ptr.get() << std::endl;
    std::cout << "Pointer Data:" << *ptr.get() << std::endl;
    takg::SmartPtr<int> ptr1;

    ptr1 = ptr;

    return;
}

#endif // SMARTPTRTEST_H_INCLUDED
