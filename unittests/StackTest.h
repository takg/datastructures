#ifndef STACKTEST_H_INCLUDED
#define STACKTEST_H_INCLUDED

#include "../include/Stack.h"

void StackTest()
{
    std::cout << "Stack Operation" << std::endl;
    takg::Stack<int> stack;

    try
    {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);

        std::cout << "Stack Copy Test " << std::endl;
        takg::Stack<int> copyStack = stack;
        std::cout << "Stack<int> contents " << copyStack << std::endl;
        copyStack.pop();
        std::cout << "Stack Assignment Test " << std::endl;
        takg::Stack<int> assignmentStack;
        assignmentStack = copyStack;
        std::cout << "Stack<int> contents " << assignmentStack << std::endl;

        std::cout << "Stack elements by pop order ";
        std::cout << stack.pop() << " ";
        std::cout << stack.pop() << " ";
        std::cout << stack.pop() << " ";
        std::cout << stack.pop() << " ";
        std::cout << stack.pop() << " " << std::endl;
        stack.pop();
    }
    catch(const takg::StackUnderflow& e)
    {
        std::cout << "Exception StackUnderflow caught:: " << e.what() << std::endl;
    }
    catch(const takg::StackOverflow& e)
    {
        std::cout << "Exception StackOverflow caught:: " << e.what() << std::endl;
    }
    catch(const std::exception& e)
    {
        std::cout << "Exception exception caught:: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cout << "Exception All caught:: " << std::endl;
    }
}


#endif // STACKTEST_H_INCLUDED
